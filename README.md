# Story 10

This Gitlab repository is the result of work from **Ahmad Izzudin Alifyandra**

## Pipeline and Coverage

[![pipeline status](https://gitlab.com/alifyandra/wdpstory10/badges/master/pipeline.svg)](https://gitlab.com/alifyandra/wdpstory10/commits/master)
[![coverage report](https://gitlab.com/alifyandra/wdpstory10/badges/master/coverage.svg)](https://gitlab.com/alifyandra/wdpstory10/commits/master)

## URL

This lab project can be accessed from [https://story10auth.herokuapp.com](https://story10auth.herokuapp.com)

## Author

**Ahmad Izzudin Alifyandra** - [alifyandra](https://gitlab.com/alifyandra)
