from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import index, register
from .apps import HomeConfig

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/',follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_using_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_register(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_index_contains_header(self):
        response = Client().get('/',follow=True)
        response_content = response.content.decode('utf-8')
        self.assertIn("<h1>Home Page</h1>", response_content)

    def test_using_index_template(self):
        response = Client().get('/',follow=True)
        self.assertTemplateUsed(response, 'home/index.html')

    def test_using_register_template(self):
        response = Client().get('/register',follow=True)
        self.assertTemplateUsed(response, 'home/register.html')

    def test_apps(self):
        self.assertEqual(HomeConfig.name, 'home')
        self.assertEqual(apps.get_app_config('home').name, 'home')

    def test_register_forms_filled(self):
        response = self.client.post('/register/', follow = True, data = {'username':'HotMan','password1':'chicky123','password2':'chicky123'})
        self.assertEqual(response.status_code, 200)